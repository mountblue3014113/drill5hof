const getEmaildetails = (dataSet) => {
  if (!Array.isArray(dataSet)) {
    throw new Error("Input must be an array.");
  }
  if (dataSet.length == 0) {
    throw new Error("Dataset is Empty");
  }

  return dataSet.map((user) => {
    if (!user.email) {
      throw new Error("missing email property.");
    }

    const emailParts = user.email.split("@");
    if (emailParts.length !== 2) {
      throw new Error("Invalid email format");
    }
    const [firstName, lastNameParts] = emailParts[0]
      .split(".")
      .map((part) => part.charAt(0).toUpperCase() + part.slice(1));
    const emailDomain = emailParts[1];
    return { firstName, lastNameParts, emailDomain };
  });
};

module.exports = getEmaildetails;
