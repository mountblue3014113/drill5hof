const groupProjectsByStatus = (dataSet) => {
  if (!Array.isArray(dataSet) || dataSet.length === 0) {
    throw new Error("dataSet is empty or not an array.");
  }
  const groupedProjects = {};

  dataSet.forEach((person) => {
    person.projects.forEach((project) => {
      if (groupedProjects[project.status]) {
        groupedProjects[project.status].push(project.name);
      } else {
        groupedProjects[project.status] = [project.name];
      }
    });
  });

  return groupedProjects;
};

module.exports = groupProjectsByStatus;
