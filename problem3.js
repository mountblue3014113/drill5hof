  // From the given data, filter all the unique langauges and return in an array
const getAllUniqueLanguages = (dataSet) => {
  if (!Array.isArray(dataSet) || dataSet.length === 0) {
    throw new Error("dataSet is empty or not an array !");
  }

  const uniqueLanguages = [...new Set(dataSet.flatMap((person) => person.languages)),
  ];

  return uniqueLanguages;
};

module.exports = getAllUniqueLanguages;
