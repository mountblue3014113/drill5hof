const getAllUniqueLanguages = require("../problem3.js");
const dataSet = require("../data.js");

try {
  console.log(getAllUniqueLanguages(dataSet));
} catch (error) {
  console.error(error.message);
}
